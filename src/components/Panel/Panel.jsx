import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import "./Panel.scss";


class Panel extends Component {

    render() {
        return (
            <div className="panel_wrapper no_select">
                <div className="panel">
                    <div className="links_wrapper">
                        <NavLink to="/main/button-page">Button</NavLink>
                        <NavLink to="/main/render-field-page">RenderField</NavLink>
                        <NavLink to="/main/custom-check-page">CustomCheck</NavLink>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ dashboard, campaigns }) => {
    return {

    };
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Panel);
