import React from 'react';
import { RadioButtonGroup } from 'material-ui/RadioButton';
import './RenderRadioField.scss';

const RenderRadioField = ({ input, ...rest }) => {
    return (
        <div className="radio_field_wrapper">
            <RadioButtonGroup
                {...input}
                {...rest}

                valueSelected={input.value}
                onChange={(event, value) => input.onChange(value)}
            />
        </div>
    );
};

export default RenderRadioField;