import React, {Component} from 'react';
import {connect} from 'react-redux';
import DefaultButton from "./DefaultButton/DefaultButton";

import './ButtonPage.scss';

class ButtonPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
        };
    }

    render(){
        const { loading } = this.state;
        if (loading) return null;
        return (
            <div className="page_wrapper button_wrapper">
                <div>
                    <span className="title">DefaultButton</span>
                    <DefaultButton
                        variant="contained"
                    >
                        Button
                    </DefaultButton>
                </div>

                <div>
                    <span className="title">DefaultButton Link</span>
                    <DefaultButton
                        variant="contained"
                        type="link"
                        to="/main/"
                    >
                        Button
                    </DefaultButton>
                </div>

                <div>
                    <span className="title">Button LINE</span>
                    <button className="btn_line">Button</button>
                </div>

                <div>
                    <span className="title">Button border color</span>
                    <button className="btn_border_color">Button</button>
                </div>

                <div>
                    <span className="title">Button color</span>
                    <button className="btn_color">Button</button>
                </div>

                <div>
                    <span className="title">Button opacity</span>
                    <button className="btn_opacity">Button</button>
                </div>

                <div>
                    <span className="title">Button scale</span>
                    <button className="btn_scale">Button</button>
                </div>

                <div>
                    <span className="title">Button rotate</span>
                    <button className="btn_rotate">Button</button>
                </div>


                <div>
                    <span className="title">Button border scale</span>
                    <button className="btn_scale_border">Button</button>
                </div>

                <div>
                    <span className="title">Button pulse</span>
                    <button className="pulse">Button</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
};
const mapDispatchToProps = {
};
export default connect(mapStateToProps, mapDispatchToProps)(ButtonPage);
