import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import renderField_mail from '../RenderFieldPage/RenderFieldIcon/renderField_mail';
import RenderField_pass from '../RenderFieldPage/RenderFieldIcon/RenderField_pass';
import RenderField from './RenderField/RenderField';
import RenderFieldEye from './RenderFieldEye/RenderFieldEye';

import './RenderFieldPage.scss';

class RenderFieldPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
        };
    }

    submitForm = data => {
        // const { postLogin, history } = this.props;
        // this.setState({ loading: true });
        // let obj = { ...data};
        // setTimeout(() => {
        //     postLogin(obj).then(res => {
        //         if(res.payload && res.payload.status && res.payload.status === 200) {
        //             localStorage.token = res.payload.data.token;
        //             history.push('/main');
        //         } else {
        //             this.setState({loading: false});
        //         }
        //     });
        // }, 500);
    };

    render(){
        const { handleSubmit, valid } = this.props;
        const { loading } = this.state;
        if (loading) return null;
        return (
            <div className="page_wrapper render_field_wrapper">
                <form className="form_info" onSubmit={handleSubmit(this.submitForm)}>
                    <Field
                        name="email1"
                        className="phone-input"
                        type="text"
                        component={renderField_mail}
                        placeholder="Электронный адрес"
                        autoComplete='off'
                    />
                    <Field
                        name="password1"
                        className="phone-input"
                        type="password"
                        component={RenderField_pass}
                        placeholder="Пароль"
                        autoComplete='off'
                    />

                    <Field
                        name="email2"
                        type="text"
                        component={RenderField}
                        label="Email"
                    />

                    <Field
                        name="password2"
                        type="password"
                        component={RenderFieldEye}
                        label="Пароль"
                        password
                    />

                </form>

            </div>
        );
    }
}

export const validate = values => {
    const errors = {};
    if (!values.email1) {
        errors.email1 = 'Обязательное поле'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)) {
        errors.email1 = 'Неправильный email'
    }

    if (!values.email2) {
        errors.email2 = 'Обязательное поле'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)) {
        errors.email2 = 'Неправильный email'
    }

    if (!values.password1) {
        errors.password1 = 'Обязательное поле'
    }
    if (!values.password2) {
        errors.password2 = 'Обязательное поле'
    }

    return errors
};

RenderFieldPage = reduxForm({
    form: 'RenderFieldForm',
    validate
})(RenderFieldPage);

const mapStateToProps = (state) => {
    return {
    }
};
const mapDispatchToProps = {
};
export default connect(mapStateToProps, mapDispatchToProps)(RenderFieldPage);
