//////////////////////////////auth actions//////////////////////////////

export const LOGIN       = 'LOGIN';
export const LOGIN_FAIL     = 'LOGIN_FAIL';

//////////////////////////////Dashboard/////////////////////////////////

export const GET_DASHBOARD      = 'GET_DASHBOARD';
export const GET_DASHBOARD_SUCCESS      = 'GET_DASHBOARD_SUCCESS';