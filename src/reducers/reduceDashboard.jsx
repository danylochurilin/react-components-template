import * as types from "../actions/constants";

const INITIAL_STATE = {
    dashboard: [],
};

export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case types.GET_DASHBOARD_SUCCESS :
            return {...state, dashboard : action.payload.data};
        default:
            return state;
    }
}