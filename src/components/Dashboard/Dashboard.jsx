import React, {Component} from 'react';
import {connect} from 'react-redux';

import { getDashboard } from '../../actions/dashboardActions';

import './Dashboard.scss';

class Dashboard extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
        };
    }

    render(){
        const { loading } = this.state;
        if (loading) return null;
        return (
            <div className="page_wrapper">
                Dashboard
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dashboard: state.dashboard.dashboard,
    }
};
const mapDispatchToProps = {
    getDashboard
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
