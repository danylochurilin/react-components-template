import * as types from './constants.jsx';

export function postLogin(data) {
    return {
        type: types.LOGIN,
        payload: {
            client: 'default',
            request: {
                url: `/auth/sign-in/`,
                method: "post",
                data
            }
        }
    };
}