import * as types from './constants.jsx';

export function getDashboard(arr) {
    return {
        type: types.GET_DASHBOARD,
        payload: {
            client: 'default',
            request: {
                url: `/dashboard/${arr.length && arr.length !== 0 ? '?' : ''}${arr.join('&')}`,
                method: "get",
            }
        }
    };
}