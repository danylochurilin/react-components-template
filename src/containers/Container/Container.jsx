import React, { Component, Fragment } from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import Dashboard from '../../components/Dashboard/Dashboard';
import Panel from '../../components/Panel/Panel';
import ButtonPage from '../../components/ButtonPage/ButtonPage';
import RenderFieldPage from '../../components/RenderFieldPage/RenderFieldPage';
import CustomCheck from '../../components/CustomCheck/CustomCheck';
class Container extends Component {

    render() {
        const { match } = this.props;
        return (
            <Fragment>
                <div className="page">
                    <Panel/>
                    <Switch>
                        <Route path={match.url} exact component={Dashboard} />
                        <Route path={`${match.url}/button-page`} exact component={ButtonPage} />
                        <Route path={`${match.url}/render-field-page`} exact component={RenderFieldPage} />
                        <Route path={`${match.url}/custom-check-page`} exact component={CustomCheck} />
                        <Route render={()=>(<p>Not found</p>)} />
                    </Switch>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    }
};
const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
