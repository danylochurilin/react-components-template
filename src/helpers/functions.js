import React, {} from "react";

export function getOption(label) {
    return (
        <div className={`status ${label}`}>
            <div>{label !== "All networks" && <span />}{label}</div>
        </div>
    )
}

export function splitByCommas(data) {
    return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}