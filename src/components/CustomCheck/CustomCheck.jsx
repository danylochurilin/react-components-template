import React, {Component} from 'react';
import {connect} from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { RadioButton } from 'material-ui/RadioButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

import RenderCheckField from "../CustomCheck/RenderCheckField/RenderCheckField";
import RenderRadioField from "../CustomCheck/RenderRadioField/RenderRadioField";
import RadioOff from '../../assets/image/CustomCheck/radio_off.png';
import RadioOn from '../../assets/image/CustomCheck/radio_on.png';
import './CustomCheck.scss';

class CustomCheck extends Component {

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            selectedValue: 'b',

        };
    }

    submitForm = data => {
        // const { postLogin, history } = this.props;
        // this.setState({ loading: true });
        // let obj = { ...data};
        // setTimeout(() => {
        //     postLogin(obj).then(res => {
        //         if(res.payload && res.payload.status && res.payload.status === 200) {
        //             localStorage.token = res.payload.data.token;
        //             history.push('/main');
        //         } else {
        //             this.setState({loading: false});
        //         }
        //     });
        // }, 500);
    };


    handleChange = event => {
        this.setState({ selectedValue: event.target.value });
    };

    render(){
        const { handleSubmit } = this.props;
        const { loading } = this.state;
        if (loading) return null;
        return (
            <div className="page_wrapper custom_check_wrapper">
                <form className="form_info" onSubmit={handleSubmit(this.submitForm)}>
                    <div>
                        <Field name="name" component={RenderCheckField} label="component check box" />
                        <div className="checkbox_wrapper">
                            <input name="checkbox" type="checkbox" className="checkbox" />
                            <span>custom input</span>
                        </div>
                    </div>
                    <div>
                        <MuiThemeProvider>
                            <Field name="sex" component={RenderRadioField}>
                                <RadioButton value="male" label="male" />
                                <RadioButton value="female" label="female" />
                            </Field>
                        </MuiThemeProvider>


                        <FormControlLabel
                            value="miss"
                            control={
                                <Radio
                                    checked={this.state.selectedValue === 'b'}
                                    onChange={this.handleChange}
                                    value="b"
                                    color="primary"
                                    checkedIcon={<img src={RadioOn} alt="radio on"/>}
                                    icon={<img src={RadioOff} alt="radio off"/>}
                                />}
                            classes={{
                                root: 'custom_radio'
                            }}
                            label="По номеру детали"
                        />
                        <FormControlLabel
                            value="miss"
                            control={
                                <Radio
                                    checked={this.state.selectedValue === 'b'}
                                    onChange={this.handleChange}
                                    value="b"
                                    color="primary"
                                    checkedIcon={<img src={RadioOn} alt="radio on"/>}
                                    icon={<img src={RadioOff} alt="radio off"/>}
                                />}
                            classes={{
                                root: 'custom_radio'
                            }}
                            label="По номеру детали"
                        />


                        <div>
                            <div className="radio_box_wrapper">
                                <input
                                    name="1"
                                    type="radio"
                                    className="radio_box"/>
                                <span>custom input 1</span>
                            </div>
                            <div className="radio_box_wrapper">
                                <input
                                    name="2"
                                    type="radio"
                                    className="radio_box"/>
                                <span>custom input 2</span>
                            </div>

                        </div>

                    </div>
                </form>
            </div>
        );
    }
}

export const validate = values => {
    const errors = {};
    if (!values.email1) {
        errors.email1 = 'Обязательное поле'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)) {
        errors.email1 = 'Неправильный email'
    }

    if (!values.email2) {
        errors.email2 = 'Обязательное поле'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)) {
        errors.email2 = 'Неправильный email'
    }

    if (!values.password1) {
        errors.password1 = 'Обязательное поле'
    }
    if (!values.password2) {
        errors.password2 = 'Обязательное поле'
    }

    return errors
};

CustomCheck = reduxForm({
    form: 'CustomCheck',
    validate
})(CustomCheck);

const mapStateToProps = (state) => {
    return {
    }
};
const mapDispatchToProps = {
};
export default connect(mapStateToProps, mapDispatchToProps)(CustomCheck);
